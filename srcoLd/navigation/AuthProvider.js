import React, { createContext, useState } from 'react';
import auth from '@react-native-firebase/auth';
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';

export const AuthContext = createContext({});

GoogleSignin.configure({
  webClientId: '423830096343-ko2qt0jq2njs2aa974pbmn25vavd40un.apps.googleusercontent.com',
  offlineAccess: true,
});

export const AuthProvider = ({ children }) => {
    const [user, setUser] = useState(null);
  
    return (
      <AuthContext.Provider
        value={{
          user,
          setUser,
          login: async (email, password) => {
            try {
              await auth().signInWithEmailAndPassword(email, password);
            } catch (e) {
              console.log(e);
            }
          },
          register: async (email, password) => {
            try {
              await auth().createUserWithEmailAndPassword(email, password);
            } catch (e) {
              console.log(e);
            }
          },
          logout: async () => {
            try {
              await auth().signOut();
            } catch (e) {
              console.error(e);
            }
          },
          googleSignIn: async () => {
            try {
              await GoogleSignin.hasPlayServices();
              const userInfo = await GoogleSignin.signIn();
              console.log(userInfo);
              setUser(userInfo);
            } catch (error) {
              if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                console.log(error.message);
                // user cancelled the login flow
              } else if (error.code === statusCodes.IN_PROGRESS) {
                console.log(error.message);
                // operation (e.g. sign in) is in progress already
              } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                console.log(error.message);
                // play services not available or outdated
              } else {
                console.log(error.message);
              }
            }
          }
        }}
      >
        {children}
      </AuthContext.Provider>
    );
  };