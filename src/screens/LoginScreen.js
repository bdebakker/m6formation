import React, {useState, useEffect, useContext} from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';
import {Title} from 'react-native-paper';
import FormInput from '../components/FormInput';
import FormButton from '../components/FormButton';
import {AuthContext} from '../navigation/AuthProvider';
import {RemoteConfigContext} from '../navigation/RemoteConfigProvider';

import {GoogleSigninButton} from '@react-native-google-signin/google-signin';

const {width, height} = Dimensions.get('screen');

export default function LoginScreen({navigation}) {
  const {login, googleSignIn, appToken} = useContext(AuthContext);
  const {isConfigLoaded, getConfigValue} = useContext(RemoteConfigContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [themeButton, setThemeButton] = useState(null);
  const [buttonIsEnable, setButtonIsEnable] = useState(false);

  useEffect(() => {
    const theme = getConfigValue('google_signin_button_theme');
    const isEnable = getConfigValue('google_signin_enabled');
    
    setThemeButton( theme == 'Dark' ? GoogleSigninButton.Color.Dark : GoogleSigninButton.Color.Light);
    setButtonIsEnable(isEnable === "true");
    isConfigLoaded && console.log({buttonIsEnable, themeButton});
  }, [isConfigLoaded])

  return (
    <View style={styles.container}>
      <Title style={styles.titleText}>Welcome to Chat app</Title>
      <FormInput
        labelName="Email"
        value={email}
        autoCapitalize="none"
        onChangeText={userEmail => setEmail(userEmail)}
      />
      <FormInput
        labelName="Password"
        value={password}
        secureTextEntry={true}
        onChangeText={userPassword => setPassword(userPassword)}
      />
      <FormButton
        title="Login"
        modeValue="contained"
        labelStyle={styles.loginButtonLabel}
        onPress={() => {
          if(email.length > 0 && password.length > 0) {
            login(email, password)
          } else {
            console.log('Email and/or password cannot be empty')
          }
        }}
      />
      {buttonIsEnable && <GoogleSigninButton
        size={GoogleSigninButton.Size.Standard}
        color={themeButton ? GoogleSigninButton.Color.Light : GoogleSigninButton.Color.Dark}
        onPress={() => googleSignIn()}
      />}
      <FormButton
        title="New user? Join here"
        modeValue="text"
        uppercase={false}
        labelStyle={styles.navButtonText}
        onPress={() => navigation.navigate('Signup')}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f5f5f5',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleText: {
    fontSize: 24,
    marginBottom: 10,
  },
  loginButtonLabel: {
    fontSize: 22,
  },
  navButtonText: {
    fontSize: 16,
    width: width,
  },
});
