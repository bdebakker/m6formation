import React, { useState, useEffect, useContext } from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity, Alert } from 'react-native';
import { List, Divider, FAB } from 'react-native-paper';
import firestore from '@react-native-firebase/firestore';
import Loading from '../components/Loading';
import useStatsBar from '../utils/useStatusBar';
import crashlytics from '@react-native-firebase/crashlytics';
import analytics from '@react-native-firebase/analytics';
import { AuthContext } from '../navigation/AuthProvider';
import messaging from '@react-native-firebase/messaging';
import dynamicLinks from '@react-native-firebase/dynamic-links';

export default function HomeScreen({ navigation }) {
  useStatsBar('light-content');

  const [threads, setThreads] = useState([]);
  const [loading, setLoading] = useState(true);
  const { user } = useContext(AuthContext);

  function handleDynamicLink(link) {
      if(link !== null){
        console.log(link);
        if(link.url === 'https://chatm6.page.link.tata'){
          Alert.alert('Bravo','Vous avez ouvert l\'application a l\'aide du deepLinking');
        }
      }
  }

  //  firebase dynamic linking listener
  useEffect(() => {
    dynamicLinks()
      .getInitialLink()
      .then((link) => {
        handleDynamicLink(link);
      });

    const linkingListener = dynamicLinks().onLink(handleDynamicLink);
    return () => {
      linkingListener();
    };
  }, []);


  requestUserPermission = async () => {
    //IOS
    const authStatus = await messaging().requestPermission();

    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    if (enabled) {
      getFcmToken() 
      console.log('Authorization status:', authStatus);
    }
  }

  useEffect(() => {
    requestUserPermission();
   }, []);

  getFcmToken = async () => {
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
     console.log(fcmToken);
     console.log("Your Firebase Token is:", fcmToken);
    } else {
     console.log("Failed", "No token received");
    }
  }

  useEffect(() => {
    const unsubscribe = messaging().onMessage(async remoteMessage => {
        if(remoteMessage.data?.userId !== user.uid){
          console.log("ON MESSAGE : "+JSON.stringify(remoteMessage));
          Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
        }
    });

    return unsubscribe;
  }, []);


  // Register background handler
  messaging().setBackgroundMessageHandler(async remoteMessage => {
      console.log('Message handled in the background!', remoteMessage);
  });

  /**
   * Fetch threads from Firestore
   */
  useEffect(() => {
    const unsubscribe = firestore()
      .collection('THREADS')
      .orderBy('latestMessage.createdAt', 'desc')
      .onSnapshot(querySnapshot => {
        const threads = querySnapshot.docs.map(documentSnapshot => {
          return {
            _id: documentSnapshot.id,
            // give defaults
            name: '',

            latestMessage: {
              text: ''
            },
            ...documentSnapshot.data()
          };
        });

        setThreads(threads);

        if (loading) {
          setLoading(false);
        }
      });

    /**
     * unsubscribe listener
     */
    return () => unsubscribe();
  }, []);

  if (loading) {
    return <Loading />;
  }

  return (
    <View style={styles.container}>
      <FlatList
        data={threads}
        keyExtractor={item => item._id}
        ItemSeparatorComponent={() => <Divider />}
        renderItem={({ item }) => (
          <TouchableOpacity
            onPress={ async () => await navigateWithAnalytics(item, navigation)}>
            <List.Item
              title={item.name}
              description={item.latestMessage.text}
              titleNumberOfLines={1}
              titleStyle={styles.listTitle}
              descriptionStyle={styles.listDescription}
              descriptionNumberOfLines={1}
            />
          </TouchableOpacity>
        )}
      />
      <FAB
        style={styles.fab}
        icon="plus"
        onPress={() => crashFunction(user.uid)}
      />
    </View>
  );
}

async function navigateWithAnalytics(item, navigation){
  await analytics().setUserProperties({
    account_type: 'gold',
    account_name: 'Gold Badge',
  });

  await analytics().logEvent('ScreenTrack', {
    id: "012345",
    item: item.name,
    description: ['desc1', 'desc2'],
  }).then(
    navigation.navigate('Room', { thread: item })
  );
}

function crashFunction(id){
  // crashlytics().log('crash on start component.');
  // crashlytics().setUserId(id);
  // crashlytics().crash()
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f5f5f5',
    flex: 1
  },
  listTitle: {
    fontSize: 22
  },
  listDescription: {
    fontSize: 16
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
});
