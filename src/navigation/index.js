import React from 'react';
import {Provider as PaperProvider} from 'react-native-paper';
import {AuthProvider} from './AuthProvider';
import {RemoteConfigProvider} from './RemoteConfigProvider';
import Routes from './Routes';

/**
 * Wrap all providers here
 */

export default function Providers() {
  return (
    <PaperProvider>
      <RemoteConfigProvider>
        <AuthProvider>
          <Routes />
        </AuthProvider>
      </RemoteConfigProvider>
    </PaperProvider>
  );
}
