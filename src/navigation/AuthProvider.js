import React, { createContext, useState } from 'react';
import auth from '@react-native-firebase/auth';

import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';

import iid from '@react-native-firebase/iid';

export const AuthContext = createContext({});

GoogleSignin.configure({
  webClientId: '980163193421-5i824qdeodm7kanj3qchlk8gvcdkfm7o.apps.googleusercontent.com',
  offlineAccess: true,
});

export const AuthProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [manualSignin, setManualSignin] = useState(true);

    return (
      <AuthContext.Provider
        value={{
          user,
          setUser,
          appToken: async () => {
            console.log({token: await iid().getToken()})
          },
          login: async (email, password) => {
            try {
              await auth().signInWithEmailAndPassword(email, password);
            } catch (e) {
              console.log(e);
            }
          },
          register: async (email, password) => {
            try {
              await auth().createUserWithEmailAndPassword(email, password);
            } catch (e) {
              console.log(e);
            }
          },
          logout: async () => {
            try {
              if(manualSignin) {
                await auth().signOut();
              } else {
                await GoogleSignin.signOut();
              }
              setUser(null);
            } catch (e) {
              console.error(e);
            }
          },
          googleSignIn: async () => {
            try {
              await GoogleSignin.hasPlayServices();
              const userInfo = await GoogleSignin.signIn();
              setUser(userInfo);
              setManualSignin(false);
            } catch (error) {
              if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                console.log(error.message);
                // user cancelled the login flow
              } else if (error.code === statusCodes.IN_PROGRESS) {
                console.log(error.message);
                // operation (e.g. sign in) is in progress already
              } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                console.log(error.message);
                // play services not available or outdated
              } else {
                console.log(error.message);
              }
            }
          }
        }
      }
    >
      {children}
    </AuthContext.Provider>
  );
};
