import React, {createContext, useState} from 'react';
import remoteConfig from '@react-native-firebase/remote-config'

export const RemoteConfigContext = createContext({});

export const RemoteConfigProvider = ({children}) => {
  const [isConfigLoaded, setIsConfigLoaded] = useState(false);

  return (
    <RemoteConfigContext.Provider
      value={{
        isConfigLoaded,
        initConfig: async () => {
          await remoteConfig().setConfigSettings({
            minimumFetchIntervalMillis: (3600 * 1000)
          });
          await remoteConfig().setDefaults({
            google_signin_enabled: 'true',
            google_signin_button_theme: 'Light',
          });
          if(__DEV__) {
            await remoteConfig().reset();
          }
          const fetchedRemotely = await remoteConfig().fetchAndActivate();
          setIsConfigLoaded(true);
          if (fetchedRemotely) {
            console.log(
              'Configs were retrieved from the backend and activated.',
            );
          } else {
            console.log(
              'No configs were fetched from the backend, and the local configs were already activated',
            );
          }
        },
        getConfigValue: (key) => {
          return remoteConfig().getValue(key).asString();
        }

      }}>
      {children}
    </RemoteConfigContext.Provider>
  );
};
